<?php
/*
Plugin Name: TWCfit Utility Functions
Plugin URI: http://geek.1bigidea.com/
Description: Handle Minor tasks for TWCfit
Version: 1.0
Author: Tom Ransom
Author URI: http://1bigidea.com
Network Only: false

Licensed under The MIT License (MIT)

Copyright 2015 Tom Ransom (email: transom@1bigidea.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

add_filter('register', 'twcfit_no_register_link');
function twcfit_no_register_link( $url ){
    return '';
}

/* Hide BuddyPress Registration Nags */
function twcfit_disable_bp_registration() {
  remove_action( 'bp_init',    'bp_core_wpsignup_redirect' );
  remove_action( 'bp_screens', 'bp_core_screen_signup' );
}
add_action( 'bp_loaded', 'twcfit_disable_bp_registration' );

add_filter( 'bp_get_signup_page', "twcfit_redirect_bp_signup_page");
    function twcfit_redirect_bp_signup_page($page ){
        return bp_get_root_domain() . '/wp-signup.php';
    }